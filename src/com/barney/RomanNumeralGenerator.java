package com.barney;

public interface RomanNumeralGenerator
{
	String generate(int number);
}
