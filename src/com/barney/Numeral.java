package com.barney;

public class Numeral {
    private String numeral;
    private int value;

    public Numeral(String numeral, int value) {
        this.numeral = numeral;
        this.value = value;
    }

    public String getNumeral() {
        return numeral;
    }

    public int getValue() {
        return value;
    }
}
