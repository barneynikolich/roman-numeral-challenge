package com.barney;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Tests {

    RomanNumeralGenerator romanNumeralGenerator;

    @BeforeEach
    void beforeEach()
    {
        romanNumeralGenerator = new RomanNumeralGeneratorImpl();
    }

    @Test
    void _9ShouldBeIX()
    {
        String numeral = romanNumeralGenerator.generate(9);
        assertEquals(numeral, "IX");
    }

    @Test
    void _4ShouldBeIV()
    {
        String numeral = romanNumeralGenerator.generate(4);
        assertEquals(numeral, "IV");
    }

    @Test
    void _40ShouldBeXL()
    {
        String numeral = romanNumeralGenerator.generate(40);
        assertEquals(numeral, "XL");
    }


    @Test
    void _400ShouldBeXL()
    {
        String numeral = romanNumeralGenerator.generate(400);
        assertEquals(numeral, "CD");
    }

    @Test
    void _900ShouldBeCM()
    {
        String numeral = romanNumeralGenerator.generate(900);
        assertEquals(numeral, "CM");
    }

    @Test
    void numbersEndingIn9ShouldEndWithIX()
    {
        assertNumeralEnding(9, "IX", 10);
    }


    @Test
    void numbersEndingIn4ShouldEndWithIV()
    {
        assertNumeralEnding(4, "IV", 10);

    }

    @Test
    void numbersEndingIn40ShouldBeXL()
    {
        assertNumeralEnding(40, "XL", 100);
    }

    @Test
    void numbersEndingIn400ShouldBeXL()
    {
        assertNumeralEnding(400, "CD", 1000);
    }

    @Test
    void numbersEndingIn900ShouldBeXL()
    {
        assertNumeralEnding(900, "CM", 1000);
    }


    private void assertNumeralEnding(int condition, String numeralCondition, int modulo)
    {
        for (int number = 1; number < 3999; number++)
        {
            int lastDigit = number % modulo;
            if (lastDigit == condition)
            {
                assertTrue(romanNumeralGenerator.generate(number).endsWith(numeralCondition));
            }
        }
    }


}
