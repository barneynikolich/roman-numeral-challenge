package com.barney;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author BarneyN
 * @since 05/04/2019
 */
public class RomanNumeralGeneratorImpl implements RomanNumeralGenerator
{
	private static Numeral I = new Numeral("I", 1);
	private static Numeral V = new Numeral("V", 5);
	private static Numeral X = new Numeral("X", 10);
	private static Numeral L = new Numeral("L", 50);
	private static Numeral C = new Numeral("C", 100);
	private static Numeral D = new Numeral("D", 500);
	private static Numeral M = new Numeral("M", 1000);

	private static List<Numeral> numerals = Arrays.asList(I, V, X, L, C, D, M);

	@Override public String generate(int number)
	{
		StringBuilder numeralBuilder = new StringBuilder();
		return toNumeral(number, numeralBuilder);
	}

	private String toNumeral(int arabicNumber, StringBuilder sb) {
		String numeral = sb.toString();

		if (arabicNumber == 0 )
		{
			return sb.toString();
		}

		if (attemptingNine(numeral))
		{
			removeIncorrectNumerals(sb, 4);
			arabicNumber += incrementToHandleRemovedNumerals(I.getValue() * 3, V.getValue());
			return toNumeral(arabicNumber + I.getValue(), sb.append(I.getNumeral()));
		}
		else if(attemptingFour(numeral))
		{
			removeIncorrectNumerals(sb, 3);
			arabicNumber += incrementToHandleRemovedNumerals((I.getValue() * 3));
			return toNumeral(arabicNumber + I.getValue(), sb.append(I.getNumeral()));
		}
		else if (attemptingForty(arabicNumber, sb))
		{
			removeIncorrectNumerals(sb, 3);
			arabicNumber += L.getValue();
			return toNumeral(arabicNumber - X.getValue(), sb.append(X.getNumeral()));
		}
		else if (attemptingNinety(arabicNumber, sb))
		{
			removeIncorrectNumerals(sb, 2);
			arabicNumber += incrementToHandleRemovedNumerals(X.getValue(), L.getValue());
			return toNumeral(arabicNumber - X.getValue(), sb.append(X.getNumeral()));
		}
		else if (attemptingFourHundred(arabicNumber, sb))
		{

			removeIncorrectNumerals(sb, 3);
			arabicNumber += incrementToHandleRemovedNumerals(D.getValue());
			return toNumeral(arabicNumber - C.getValue(), sb.append(C.getNumeral()));
		}
		else if (attemptingNineHundred(arabicNumber, sb))
		{
			removeIncorrectNumerals(sb, 2);
			arabicNumber += incrementToHandleRemovedNumerals(C.getValue(), D.getValue());
			return toNumeral(arabicNumber - C.getValue(), sb.append(C.getNumeral()));
		}
		else // not special case
		{
			Numeral currentNumeral = findNextHighestDivider(arabicNumber);
			return toNumeral(arabicNumber - currentNumeral.getValue(), sb.append(currentNumeral.getNumeral()));
		}
	}

	private void removeIncorrectNumerals(StringBuilder sb, int incorrectCount)
	{
		sb.setLength(sb.length() - incorrectCount);
	}

	private int incrementToHandleRemovedNumerals(int... values)
	{
		return IntStream.of(values).sum();
	}

	private static Numeral findNextHighestDivider(int number)
	{
		int currentDivisor = number / I.getValue(); // Always start with I - first attempt for highest divisor
		Numeral highestDivisor = null;

		for (Numeral numeral : numerals) {
			int attempt = number / numeral.getValue();
			if (attempt <= currentDivisor && attempt != 0)
			{
				highestDivisor = numeral;
				currentDivisor = attempt;
			}
			if (attempt == 1 || attempt == 0)
			{
				break; // Found highest divisor - Divides only once
			}
		}
		return highestDivisor;
	}

	private boolean attemptingFour(String numeral)
	{
		return numeral.contains("III");
	}

	private boolean attemptingNine(String numeral)
	{
		return numeral.endsWith("VIII");
	}

	private boolean attemptingForty(int arabicNumber, StringBuilder sb)
	{
		return isPredicateMet("XXX", "X", sb.toString(), arabicNumber);
	}

	private boolean attemptingNinety(int arabicNumber, StringBuilder sb)
	{
		return isPredicateMet("LX", "L", sb.toString(), arabicNumber);
	}

	private boolean attemptingFourHundred(int arabicNumber, StringBuilder sb)
	{
		return isPredicateMet("CCC", "C", sb.toString(), arabicNumber);
	}

	private boolean attemptingNineHundred(int arabicNumber, StringBuilder sb)
	{
		return isPredicateMet("DC", "D", sb.toString(), arabicNumber);
	}

	private boolean isPredicateMet(String endingCondition, String nextNumeral, String numeral, int arabicNumber)
	{
		return numeral.endsWith(endingCondition) && findNextHighestDivider(arabicNumber).getNumeral().equals(nextNumeral);
	}
}
